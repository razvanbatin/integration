package com.cst.endpoint;

import com.cst.model.ProductInformation;
import com.cst.model.ProductPrice;
import com.cst.model.ProductStock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;

@RestController()
public class ProductController {


    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @PostMapping("/info")
    @ResponseBody
    ProductInformation addInfo(@RequestBody ProductInformation productInformation) {

        logger.info("Received object: " + productInformation);

        return productInformation;
    }

    @PostMapping("/stock")
    @ResponseBody
    ProductStock addStock(@RequestBody ProductStock productStock) {

        logger.info("Received object: " + productStock);

        return productStock;
    }

    @PostMapping("/price")
    @ResponseBody
    ProductPrice addPrice(@RequestBody ProductPrice productPrice, HttpServletRequest request) {


        printRequest(request);
        logger.info("Received object: ");


        return productPrice;
    }


    private void printRequest(HttpServletRequest httpRequest) {
        logger.info(" \n\n Headers");

        Enumeration headerNames = httpRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            logger.info(headerName + " = " + httpRequest.getHeader(headerName));
        }

        logger.info("\n\nParameters");

        Enumeration params = httpRequest.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = (String) params.nextElement();
            logger.info(paramName + " = " + httpRequest.getParameter(paramName));
        }

        logger.info("\n\n Row data");
        logger.info(extractPostRequestBody(httpRequest));
    }

    static String extractPostRequestBody(HttpServletRequest request) {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }

}
