package com.cst.model;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class ProductInformation {

    /*

    {
    "id": "123",
    "sku": "PRD-222",
    "name": "Lenovo ThinkPad 1234",
    "tags": [
        {
            "key": "value"
        }
    ],
    "ts": "2020-06-12 12:00:00"
    }

     */

    @NotNull
    private String id;

    @NotNull
    private String sku;

    @NotNull
    private String name;

    @NotNull
    private String ts;


    private String tags;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getTags() {
        return tags;
    }

    public void setTags( String tags) {
        this.tags = tags;
    }


}
