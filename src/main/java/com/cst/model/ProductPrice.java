package com.cst.model;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class ProductPrice {

    /*

    {
        "id": "123",
        "sku": "PRD-888",
        "price": 120.23,
        "channel": "WEB_DE",
        "validFrom": 1565187870,
        "validTo": 1565187870,
        "tags": [],
        "ts": "YYYY-MM-DD HH:MM:SS"
    }

    */



    @NotNull
    private String id;

    @NotNull
    private String sku;

    @NotNull
    private Double price;

    @NotNull
    private String channel;

    private Long validFrom;

    private Long validTo;

    private String tags;

    @NotNull
    private String ts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public String getTags() {
        return tags;
    }

    public void setTags( String tags) {
        this.tags = tags;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
