package com.cst.model;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class ProductStock {

    /*

    {
        "id": "123",
        "sku": "PRD-12345",
        "stockId": "ST_KO",
        "quantity": 3,
        "openPurchaseOrderQuantity": 20,
        "reservedQuantity": 2,
        "stockInTransfer": 10,
        "reorderLevel": 20,
        "replenishmentQuantity": 10,
        "averagePurchasePrice": 1.01,
        "tags": [
            {
                "key": "value"
            }
        ],
        "ts": "YYYY-MM-DD HH:MM:SS"
    }

    */

    @NotNull
    private String id;

    @NotNull
    private String sku;

    @NotNull
    private String stockId;

    @NotNull
    private Integer quantity;

    @NotNull
    private Integer openPurchaseOrderQuantity;

    @NotNull
    private Integer reservedQuantity;

    @NotNull
    private Integer stockInTransfer;

    @NotNull
    private Integer replenishmentQuantity;

    @NotNull
    private Double averagePurchasePrice;

    private String tags;

    @NotNull
    private String ts;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getOpenPurchaseOrderQuantity() {
        return openPurchaseOrderQuantity;
    }

    public void setOpenPurchaseOrderQuantity(Integer openPurchaseOrderQuantity) {
        this.openPurchaseOrderQuantity = openPurchaseOrderQuantity;
    }

    public Integer getReservedQuantity() {
        return reservedQuantity;
    }

    public void setReservedQuantity(Integer reservedQuantity) {
        this.reservedQuantity = reservedQuantity;
    }

    public Integer getStockInTransfer() {
        return stockInTransfer;
    }

    public void setStockInTransfer(Integer stockInTransfer) {
        this.stockInTransfer = stockInTransfer;
    }

    public Integer getReplenishmentQuantity() {
        return replenishmentQuantity;
    }

    public void setReplenishmentQuantity(Integer replenishmentQuantity) {
        this.replenishmentQuantity = replenishmentQuantity;
    }

    public Double getAveragePurchasePrice() {
        return averagePurchasePrice;
    }

    public void setAveragePurchasePrice(Double averagePurchasePrice) {
        this.averagePurchasePrice = averagePurchasePrice;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }
}
